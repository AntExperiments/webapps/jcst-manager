import NextCors from 'nextjs-cors'
import fs from 'fs'

export default async function handler(req, res) {

    await NextCors(req, res, {
      methods: ['GET'],
      origin: '*',
      optionsSuccessStatus: 200,
   });

   res.json({ timeOffset: await fs.readFileSync('public/JCST.txt').toString() });
}