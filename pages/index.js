import Head from 'next/head'
import styles from '../styles/Home.module.css'
import moment from 'moment'
import { useState } from 'react'

export default function Home({ initialOffset }) {
  const [ offset, setOffset ] = useState(initialOffset)
  
  const localTime = moment()
  const jcsTime = moment().utc().add(offset, 'hours')

  const sendTimeForUpdate = () => {
    const password = document.querySelector("#offsetPassword").value
    const newTimeOffset = document.querySelector("#offsetInput").value
    
    console.info(`updating offset to ${newTimeOffset}`)
  
    fetch('api/update', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        newTimeOffset,
        password
      }),
    })
    .then(e => e.status == "200" || alert(e.statusText))

    setOffset(newTimeOffset)
  }

  return <>
    <div className={styles.background}></div>
    <div className={styles.container}>
      <Head>
        <title>JCST - Manager</title>
        <link rel="icon" href="https://cdn.discordapp.com/avatars/275403156945502219/c207828ce8caab749a726078bea4f83b.png" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          JCST Manager
        </h1>

        <section>
          <span>Local time: {localTime.format("HH:mm")}</span><br/>
          <span>JCST time: {jcsTime.format("HH:mm")}</span>
        </section>

        <section className={styles.input}>
          <input type="password" id="offsetPassword" placeholder="Password"/>
          <div>
            <input type="number" id="offsetInput" placeholder="Time Offset (in hours)" />
            <a onClick={sendTimeForUpdate}>⏎</a>
          </div>
        </section>

      </main>
    </div>
  </>
}


Home.getInitialProps = async () => {
  const res = await fetch('http://localhost:3000/JCST.txt')
  const initialOffset = await res.text()
  return { initialOffset }
}